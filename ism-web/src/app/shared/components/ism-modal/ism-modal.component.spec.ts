import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IsmModalComponent } from './ism-modal.component';

describe('IsmModalComponent', () => {
  let component: IsmModalComponent;
  let fixture: ComponentFixture<IsmModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsmModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
