import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IsmGridComponent } from './ism-grid.component';

describe('IsmGridComponent', () => {
  let component: IsmGridComponent;
  let fixture: ComponentFixture<IsmGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsmGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsmGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
