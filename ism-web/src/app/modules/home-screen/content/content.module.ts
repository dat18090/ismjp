import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule } from '@angular/router';
import { C0HomeComponent } from './c0-home/c0-home.component';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ContentComponent,
        children: [
          {
            path: '',
            component: C0HomeComponent
          },
        ]
      }
    ])
  ]
})
export class ContentModule { }
