/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:09:55 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ims.co.controller;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ism.co.controllers.CategoryController;
import com.ism.co.services.ICategoryService;

/**
 * The Class CategoryCreateTestControllerTest.
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(CategoryController.class)
@AutoConfigureMockMvc(addFilters = false) //for disabling security, remove if not needed
public class CategoryControllerTest {
	
    @MockBean
    private ICategoryService categoryService;

//    @Autowired
//    private CategoryConverter categoryConverter;
//
//    @Autowired
//    private MockMvc mockMvc;
}
