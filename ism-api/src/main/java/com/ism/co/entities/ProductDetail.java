/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:52:58 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class ProductDetail.
 */
@Entity(name = "product_detail")
@Table(name = "product_detail")
@Getter
@Setter
public class ProductDetail extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_detail_code")
	private long productDetailCode;
	
	@Column(name = "product_code")
	private long productCode;
	
	@Column(name = "available_product_number")
	private int availableProductNumber;
	
	@Column(name = "shop_code")
	private long shopCode;
	
	@Column(name = "description_detail")
	private String descriptionDetail;
	
	@Column(name = "transport_to")
	private String transportTo;
}
