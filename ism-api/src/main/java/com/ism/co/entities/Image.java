/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:52:19 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Image.
 */
@Entity(name = "image")
@Table(name = "image")
@Getter
@Setter
public class Image extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long imageCode;
	
	@Column(name = "image_type")
	private String imageType;
	
	@Column(name = "image_name")
	private String imageName;
	
	@ManyToOne()
	@JoinColumn(name = "product_code", insertable = false, updatable = false, 
			foreignKey = @ForeignKey(name = "fk_product_image"))
	@JsonIgnore
	private Product productImg;
}
