/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:10:13 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.converter;

import org.springframework.stereotype.Component;

import com.ism.co.controllers.request.PagingDataRequest;
import com.ism.co.controllers.request.category.CategoryCreateRequest;
import com.ism.co.controllers.request.category.CategoryDetailRequest;
import com.ism.co.controllers.response.PagingDataResponse;
import com.ism.co.controllers.response.category.CategoryCreateResponse;
import com.ism.co.controllers.response.category.CategoryDetailResponse;
import com.ism.co.controllers.response.category.CategoryListResponse;
import com.ism.co.dto.input.PagingDataInputDto;
import com.ism.co.dto.input.category.CategoryCreateInputDto;
import com.ism.co.dto.input.category.CategoryDetailInputDto;
import com.ism.co.dto.output.PagingDataOutputDto;
import com.ism.co.dto.output.category.CategoryCreateOutputDto;
import com.ism.co.dto.output.category.CategoryDetailOutputDto;
import com.ism.co.dto.output.category.CategoryListOutputDto;
import com.ism.co.entities.Category;

/**
 * The Class CategoryConverter.
 */
@Component
public class CategoryConverter {
	
	/**
	 * Convert input dto to entity on service to create data.
	 *
	 * @param object the object
	 * @return the category
	 */
	public Category convertInputDtoToEntityOnServiceToCreateData(
			final CategoryCreateInputDto object) {
		Category category = new Category();
		
		category.setCategoryName(object.getCategoryName());
		category.setSeoURL(object.getSeoURL());
		category.setImageURL(object.getImageURL());
		category.setIsDisplay(object.getIsDisplay());
		category.setRecordCreatedBy(object.getRecordCreatedBy());
		category.setRecordUpdatedBy(object.getRecordCreatedBy());
		
		return category;
	}

	/**
	 * Convert entity to output dto on service to display data when create.
	 *
	 * @param object the object
	 * @return the category create output dto
	 */
	public CategoryCreateOutputDto convertEntityToOutputDtoOnServiceToDisplayDataWhenCreate(
			final Category object) { 
		CategoryCreateOutputDto categoryOutputDto = new CategoryCreateOutputDto();
		
		categoryOutputDto.setCategoryCode(object.getCategoryCode());
		categoryOutputDto.setCategoryName(object.getCategoryName());
		categoryOutputDto.setSeoURL(object.getSeoURL());
		categoryOutputDto.setImageURL(object.getImageURL());
		categoryOutputDto.setIsDisplay(object.getIsDisplay());
		categoryOutputDto.setRecordCreatedAt(object.getRecordCreatedAt());
		categoryOutputDto.setRecordCreatedBy(object.getRecordCreatedBy());
		categoryOutputDto.setRecordUpdatedAt(object.getRecordUpdatedAt());
		categoryOutputDto.setRecordUpdatedBy(object.getRecordUpdatedBy());
		categoryOutputDto.setDeleteStatus(object.isDeleteStatus());
		
		return categoryOutputDto;
	}

	/**
	 * Convert request body to input dto on controller to create data.
	 *
	 * @param categoryCreateRequest the category create request
	 * @return the category create input dto
	 */
	public CategoryCreateInputDto convertRequestBodyToInputDtoOnControllerToCreateData(
			final CategoryCreateRequest categoryCreateRequest) {
		CategoryCreateInputDto categoryCreateInputDto = new CategoryCreateInputDto();
		
		categoryCreateInputDto.setCategoryName(categoryCreateRequest.getCategoryName());
		categoryCreateInputDto.setSeoURL(categoryCreateRequest.getSeoURL());
		categoryCreateInputDto.setImageURL(categoryCreateRequest.getImageURL());
		categoryCreateInputDto.setRecordCreatedBy(categoryCreateRequest.getRecordCreatedBy());
		categoryCreateInputDto.setRecordUpdatedBy(categoryCreateRequest.getRecordCreatedBy());
		categoryCreateInputDto.setDeleteStatus(false);
		categoryCreateInputDto.setIsDisplay("yes");
		
		return categoryCreateInputDto;
	}
	
	/**
	 * Convert output dto to response body on controller to create data when create.
	 *
	 * @param categoryCreateOutputDto the category create output dto
	 * @return the category create response
	 */
	public CategoryCreateResponse convertOutputDtoToResponseBodyOnControllerToCreateDataWhenCreate( 
			final CategoryCreateOutputDto categoryCreateOutputDto) {
		CategoryCreateResponse categoryCreateResponse = new CategoryCreateResponse();
		
		categoryCreateResponse.setCategoryCode(categoryCreateOutputDto.getCategoryCode());
		categoryCreateResponse.setCategoryName(categoryCreateOutputDto.getCategoryName());
		categoryCreateResponse.setSeoURL(categoryCreateOutputDto.getSeoURL());
		categoryCreateResponse.setImageURL(categoryCreateOutputDto.getImageURL());
		categoryCreateResponse.setIsDisplay(categoryCreateOutputDto.getIsDisplay());
		categoryCreateResponse.setRecordCreatedAt(categoryCreateOutputDto.getRecordCreatedAt());
		categoryCreateResponse.setRecordCreatedBy(categoryCreateOutputDto.getRecordCreatedBy());
		categoryCreateResponse.setRecordUpdatedAt(categoryCreateOutputDto.getRecordUpdatedAt());
		categoryCreateResponse.setRecordUpdatedBy(categoryCreateOutputDto.getRecordUpdatedBy());
		categoryCreateResponse.setDeleteStatus(categoryCreateOutputDto.isDeleteStatus());
		
		return categoryCreateResponse;
	}
	
	/**
	 * Convert input dto to entity on service to get data by code.
	 *
	 * @param object the object
	 * @return the category
	 */
	public Category convertInputDtoToEntityOnServiceToGetDataByCode(
			final CategoryDetailInputDto object) {
		Category category = new Category();
		
		category.setCategoryCode(object.getCategoryCode());
		
		return category;
	}
	
	/**
	 * Convert entity to output dto on service to display data when get from code.
	 *
	 * @param category the category
	 * @return the category detail output dto
	 */
	public CategoryDetailOutputDto convertEntityToOutputDtoOnServiceToDisplayDataWhenGetFromCode(
			final Category category) { 
		CategoryDetailOutputDto categoryDetailOutputDto = new CategoryDetailOutputDto();
		
		categoryDetailOutputDto.setCategoryCode(category.getCategoryCode());
		categoryDetailOutputDto.setCategoryName(category.getCategoryName());
		categoryDetailOutputDto.setSeoURL(category.getSeoURL());
		categoryDetailOutputDto.setImageURL(category.getImageURL());
		categoryDetailOutputDto.setRecordCreatedAt(category.getRecordCreatedAt());
		categoryDetailOutputDto.setRecordCreatedBy(category.getRecordCreatedBy());
		categoryDetailOutputDto.setRecordUpdatedAt(category.getRecordUpdatedAt());
		categoryDetailOutputDto.setRecordUpdatedBy(category.getRecordUpdatedBy());
		categoryDetailOutputDto.setDeleteStatus(category.isDeleteStatus());
		
		return categoryDetailOutputDto;
	}
	
	/**
	 * Convert request body to input dto on controller to get data by code.
	 *
	 * @param object the object
	 * @return the category detail input dto
	 */
	public CategoryDetailInputDto convertRequestBodyToInputDtoOnControllerToGetDataByCode(
			final CategoryDetailRequest object) {
		CategoryDetailInputDto category = new CategoryDetailInputDto();
		
		category.setCategoryCode(object.getCategoryCode());
		
		return category;
	}
	
	/**
	 * Convert output dto to response body on controller to get data by code.
	 *
	 * @param object the object
	 * @return the category detail response
	 */
	public CategoryDetailResponse convertOutputDtoToResponseBodyOnControllerToGetDataByCode(
			final CategoryDetailOutputDto object) { 
		CategoryDetailResponse categoryDetailResponse = new CategoryDetailResponse();
		
		categoryDetailResponse.setCategoryCode(object.getCategoryCode());
		categoryDetailResponse.setCategoryName(object.getCategoryName());
		categoryDetailResponse.setSeoURL(object.getSeoURL());
		categoryDetailResponse.setImageURL(object.getImageURL());
		categoryDetailResponse.setIsDisplay(object.getIsDisplay());
		categoryDetailResponse.setRecordCreatedAt(object.getRecordCreatedAt());
		categoryDetailResponse.setRecordCreatedBy(object.getRecordCreatedBy());
		categoryDetailResponse.setRecordUpdatedAt(object.getRecordUpdatedAt());
		categoryDetailResponse.setRecordUpdatedBy(object.getRecordUpdatedBy());
		categoryDetailResponse.setDeleteStatus(object.isDeleteStatus());
		
		return categoryDetailResponse;
	}
	
	/**
	 * Get list convert entity to output dto.
	 *
	 * @param category the category
	 * @return the category list output dto
	 */
	public CategoryListOutputDto convertEntityToOutputDtoOnServiceToGetDataActivePaging(
			final Category category) { 
		CategoryListOutputDto categoryListOutputDto = new CategoryListOutputDto();
		
		categoryListOutputDto.setCategoryCode(category.getCategoryCode());
		categoryListOutputDto.setCategoryName(category.getCategoryName());
		categoryListOutputDto.setSeoURL(category.getSeoURL());
		categoryListOutputDto.setImageURL(category.getImageURL());
		categoryListOutputDto.setDeleteStatus(category.isDeleteStatus());
		categoryListOutputDto.setIsDisplay(category.getIsDisplay());
		categoryListOutputDto.setRecordCreatedAt(category.getRecordCreatedAt());
		categoryListOutputDto.setRecordCreatedBy(category.getRecordCreatedBy());
		categoryListOutputDto.setRecordUpdatedAt(category.getRecordUpdatedAt());
		categoryListOutputDto.setRecordUpdatedBy(category.getRecordUpdatedBy());

		return categoryListOutputDto;
	}
	
	/**
	 * Convert request body to input dto on controller to handle paging.
	 *
	 * @param model the model
	 * @return the paging data input dto
	 */
	public PagingDataInputDto convertRequestBodyToInputDtoOnControllerToHandlePaging(
			final PagingDataRequest model) {
		PagingDataInputDto pagingDataInputDto = new PagingDataInputDto();
		
		pagingDataInputDto.setOffset(model.getOffset());
		pagingDataInputDto.setLimit(model.getLimit());
		pagingDataInputDto.setSortDir(model.getSortDir());
		pagingDataInputDto.setSortField(model.getSortField());
		
		return pagingDataInputDto;
	}
	
	/**
	 * Convert ouput dto to response body on controller to get data paging.
	 *
	 * @param model the model
	 * @return the paging data response
	 */
	public PagingDataResponse<CategoryListResponse> convertOuputDtoToResponseBodyOnControllerToGetDataPaging(
			final PagingDataOutputDto<CategoryListOutputDto> model) {
		PagingDataResponse<CategoryListResponse> response = new PagingDataResponse<CategoryListResponse>();
		
		response.setOffset(model.getOffset());
		response.setLimit(model.getLimit());
		response.setSortDir(model.getSortDir());
		response.setSortField(model.getSortField());
		response.setTotalPage(model.getTotalPage());
		
		return response;
	}
	
	/**
	 * Convert output dto to response body on controller to get list result category paging.
	 *
	 * @param model the model
	 * @return the category list response
	 */
	public CategoryListResponse convertOutputDtoToResponseBodyOnControllerToGetListResultCategoryPaging (
			final CategoryListOutputDto model) {
		CategoryListResponse categoryListResponse = new CategoryListResponse();
		
		categoryListResponse.setCategoryCode(model.getCategoryCode());
		categoryListResponse.setCategoryName(model.getCategoryName());
		categoryListResponse.setSeoURL(model.getSeoURL());
		categoryListResponse.setImageURL(model.getImageURL());
		categoryListResponse.setDeleteStatus(model.isDeleteStatus());
		categoryListResponse.setIsDisplay(model.getIsDisplay());
		categoryListResponse.setRecordCreatedAt(model.getRecordCreatedAt());
		categoryListResponse.setRecordCreatedBy(model.getRecordCreatedBy());
		categoryListResponse.setRecordUpdatedAt(model.getRecordUpdatedAt());
		categoryListResponse.setRecordUpdatedBy(model.getRecordUpdatedBy());
		
		return categoryListResponse;
	}
	
}
