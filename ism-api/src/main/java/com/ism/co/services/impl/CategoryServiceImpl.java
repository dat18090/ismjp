/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:10:05 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.services.impl;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.ism.co.converter.CategoryConverter;
import com.ism.co.dto.input.PagingDataInputDto;
import com.ism.co.dto.input.category.CategoryCreateInputDto;
import com.ism.co.dto.input.category.CategoryDetailInputDto;
import com.ism.co.dto.output.PagingDataOutputDto;
import com.ism.co.dto.output.category.CategoryCreateOutputDto;
import com.ism.co.dto.output.category.CategoryDetailOutputDto;
import com.ism.co.dto.output.category.CategoryListOutputDto;
import com.ism.co.entities.Category;
import com.ism.co.repositories.ICategoryRepository;
import com.ism.co.services.ICategoryService;

/**
 * The Class CategoryServiceImpl.
 */
@Service
public class CategoryServiceImpl implements ICategoryService {

	@Autowired 
	private ICategoryRepository categoryRepository;
	
	@Autowired
	private CategoryConverter categoryConverter;
	
	/**
	 * Insert data category.
	 *
	 * @param object the object
	 * @return the category create input dto
	 */
	@Override
	public CategoryCreateOutputDto insertDataCategory(CategoryCreateInputDto object) {
		Category categoryCreateInput = categoryConverter.
				convertInputDtoToEntityOnServiceToCreateData(object);
		
		// When convert data from input to Category
		categoryCreateInput.setIsDisplay("yes");
		Category categoryJPA = categoryRepository.save(categoryCreateInput);
		
		// Get data when insert
		Category dataCategoryResponseJPA = categoryRepository.getDataByPrimaryKey(
				categoryJPA.getCategoryCode());
		
		// Get data when after insert
		CategoryCreateOutputDto categoryOutputDto = categoryConverter.
				convertEntityToOutputDtoOnServiceToDisplayDataWhenCreate(dataCategoryResponseJPA);
		
		return categoryOutputDto;
	}

	/**
	 * Gets the data category by primary key.
	 *
	 * @param object the object
	 * @return the data category by primary key
	 */
	@Override
	public CategoryDetailOutputDto getDataCategoryByPrimaryKey(CategoryDetailInputDto object) {
		CategoryDetailOutputDto categoryDetailOutputDto = new CategoryDetailOutputDto();
		
		// Convert detail input dto to entity
		Category categoryInputDetail = categoryConverter.
				convertInputDtoToEntityOnServiceToGetDataByCode(object);
		
		// Get data category by primary key from code
		Category categoryJPA = categoryRepository.
				getDataByPrimaryKey(categoryInputDetail.getCategoryCode());
		
		// Convert detail data category from entity jpa to output
		categoryDetailOutputDto = categoryConverter.
				convertEntityToOutputDtoOnServiceToDisplayDataWhenGetFromCode(categoryJPA);
		
		return categoryDetailOutputDto;
	}

	/**
	 * Total item.
	 *
	 * @return the int
	 */
	@Override
	public int totalItem() {
		return (int) categoryRepository.totalItemActivePaging();
	}

	/**
	 * Find all data with paging.
	 *
	 * @param model the model
	 * @return the list
	 */
	@Override
	public PagingDataOutputDto<CategoryListOutputDto> findAllDataWithPaging(PagingDataInputDto model) {
		PagingDataOutputDto<CategoryListOutputDto> pagingDataOutputDto = new PagingDataOutputDto<CategoryListOutputDto>();
		// Create paging for handle
		Pageable pageable = PageRequest.of(model.getOffset() - 1, model.getLimit(),
				model.getSortDir().equals("asc") 
						? Sort.by(model.getSortField()).ascending() 
						: Sort.by(model.getSortField()).descending());
		
		// Create empty list to contains list from data repository
		List<CategoryListOutputDto> results = new LinkedList<CategoryListOutputDto>();

		
		// List data when get from repository
		List<Category> categories = categoryRepository.getAllDataCategoryActive(pageable);
		
		// When get data from repository then put on empty list and return list
		for (Category item : categories) {
			CategoryListOutputDto categoryDTO = categoryConverter.
					convertEntityToOutputDtoOnServiceToGetDataActivePaging(item);
			results.add(categoryDTO);
		}
		
		// Set data on model paging output dto
		pagingDataOutputDto.setOffset(model.getOffset());
		pagingDataOutputDto.setLimit(model.getLimit());
		pagingDataOutputDto.setSortDir(model.getSortDir());
		pagingDataOutputDto.setSortField(model.getSortField());
		pagingDataOutputDto.setResults(results);
		pagingDataOutputDto.setTotalPage(this.totalItem());
		
		return pagingDataOutputDto;
	}

	/**
	 * Check exists data category.
	 *
	 * @param categoryName the category name
	 * @return the int
	 */
	@Override
	public int checkExistsDataCategory(String categoryName) {
		return (int) categoryRepository.checkExistsDataCategory(categoryName);
	}

	/**
	 * Delete data category logic.
	 *
	 * @param categoryCode the category code
	 * @return the int
	 */
	@Override
	public int deleteDataCategoryLogic(boolean deleteStatus, long categoryCode) {
		return categoryRepository.deleteDataCategoryLogic(deleteStatus, categoryCode);
	}

	/**
	 * Delete more data category logic.
	 *
	 * @param deleteStatus the delete status
	 * @param categoryCodes the category codes
	 * @return the int
	 */
	@Override
	public void deleteMoreDataCategoryLogic(boolean deleteStatus, long[] categoryCodes) {
		for (long item : categoryCodes) {
			categoryRepository.deleteDataCategoryLogic(deleteStatus, item);
		}
	}
	
}
