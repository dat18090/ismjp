/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:53 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.dto.input;

import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * The Class PagingDataInputDto.
 */
@Component

/**
 * Instantiates a new paging data input dto.
 */
@Data
public class PagingDataInputDto {
	
	/** The offset. */
	private int offset;
	
	/** The limit. */
	private int limit;
	
	/** The sort field. */
	private String sortField;
	
	/** The sort dir. */
	private String sortDir;

}
