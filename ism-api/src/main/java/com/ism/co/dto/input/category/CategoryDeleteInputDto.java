/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 6:10:13 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.dto.input.category;

import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * The Class CategoryDeleteInputDto.
 */
@Component

/**
 * Instantiates a new category delete input dto.
 */
@Data
public class CategoryDeleteInputDto {

	/** The category code. */
	private long categoryCode;
	
}
