/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:33:08 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ism.co.commons.URLCommon;

/**
 * The Class TestController.
 */
@CrossOrigin
@RestController
@EnableAsync
public class TestController {
	
	/**
	 * Gets the data test.
	 *
	 * @return the data test
	 */
	@GetMapping(URLCommon.URL_ROOT + "/test/get-data-test")
	public String getDataTest() {
		return "test deployeeeee";
	}
	
}
