/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 8:32:42 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers.response.file;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UploadFileResponse {
	
	@JsonProperty("file_name")
	private String fileName;
	
	@JsonProperty("file_show_data")
    private String fileDownloadUri;
	
	@JsonProperty("file_type")
    private String fileType;
	
	@JsonProperty("file_size")
    private long size;
	
	@JsonProperty("file_unknown")
    private String fileShow;

    public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size, String fileShow) {
		this.fileName = fileName;
		this.fileDownloadUri = fileDownloadUri;
		this.fileType = fileType;
		this.size = size;
		this.fileShow = fileShow;
	}
    
	public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size) {
		this.fileName = fileName;
		this.fileDownloadUri = fileDownloadUri;
		this.fileType = fileType;
		this.size = size;
	}

}
