/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:53 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.controllers.request.category;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The Class CategoryListRequest.
 */
@Component
@Data
public class CategoryListRequest {
	
	/** The offset. */
	@JsonProperty("offset")
	private int offset;
	
	/** The limit. */
	@JsonProperty("limit")
	private int limit;
	
	/** The sort field. */
	@JsonProperty("sort_field")
	private String sortField;
	
	/** The sort dir. */
	@JsonProperty("sort_dir")
	private String sortDir;

}
