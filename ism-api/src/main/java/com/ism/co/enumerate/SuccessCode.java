/**
 * ================================================================================
 * システム名		: ism-api
 * バージョン 		: 1.00
 * 作成日		: Feb 19, 2022 3:51:53 PM
 * 説明文		: HELLO
 * Copyright (c) 2022 by ISM. All rights reserved.
 * ================================================================================
 */
package com.ism.co.enumerate;

public enum SuccessCode {
	insertSuccess("2000", "Insert data success!", "OK"), 
	getDataSuccess("2001", "Get data success!", "OK"), 
	deleteDataSuccess("2002", "Delete data success!", "OK"), 
	updateSuccess("2003", "", "ERROR");
    
	private String code;
	private String message;
	private String status;
	 
	SuccessCode(String code, String message, String status) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getStatus() {
		return status;
	}
	
}
