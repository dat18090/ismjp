import firebase from 'firebase/app';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyDGtoZphru5-XI1PEjKNh-SKbhyLqatt-I",
    authDomain: "ismjp-admin.firebaseapp.com",
    projectId: "ismjp-admin",
    storageBucket: "ismjp-admin.appspot.com",
    messagingSenderId: "438603264884",
    appId: "1:438603264884:web:7f1b256cf0a7651a44f221",
    measurementId: "G-6CFZH0SBXF"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export {
    storage, firebase as default
}