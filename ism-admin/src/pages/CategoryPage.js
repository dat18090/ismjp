import * as React from "react";
import Navbar from '../components/Navbar';
import Menu from '../components/Menu';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useState, useEffect, useRef } from 'react';
import * as moment from 'moment';
import axios from 'axios';
import Checkbox from '@mui/material/Checkbox';
import Pagination from '@mui/material/Pagination';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import DriveFileRenameOutlineIcon from '@mui/icons-material/DriveFileRenameOutline';
import BrowseGalleryIcon from '@mui/icons-material/BrowseGallery';
import AddIcon from '@mui/icons-material/Add';
import Button from '@mui/material/Button';
import FilterListIcon from '@mui/icons-material/FilterList';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { handleString } from '../common/handle_string';
import swal from 'sweetalert';
// import { storage } from "../../firebase";

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

function CategoryPage() {
    // Set state for insert
    const [categoryCode, setCategoryCode] = useState(0);
    const [categoryName, setCategoryName] = useState('');
    const [seoUrl, setSeoUrl] = useState('');
    const [imageUrl, setImageUrl] = useState('');
    const [recordCreatedBy, setRecordCreatedBy] = useState('');

    // Set state for paging find list data
    const [offset, setOffset] = useState(1);
    const [limit, setLimit] = useState(5);
    const [sortDir, setSortDir] = useState('desc');
    const [sortField, setSortField] = useState('category_code');
    const [totalPage, setTotalPage] = useState(0);

    // List data category paging
    const [categories, setCategories] = useState([]);

    // List code for delete
    const [ids, setIds] = useState([]);
    const [checked, setChecked] = useState([]);

    // Set loading
    let [loading, setLoading] = useState(true);

    // Set image picker
    const [selectedFile, setSelectedFile] = useState();
    const [isFilePicked, setIsFilePicked] = useState(false);
    const ref = useRef();

    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     * @param {*} paramOffset 
     * @param {*} paramLimit 
     * @param {*} paramSortDir 
     * @param {*} paramSortField 
     */
    const findDataCategory = (paramOffset, paramLimit, paramSortDir, paramSortField) => {
        const paging = {
            "offset": paramOffset,
            "limit": paramLimit,
            "sort_dir": paramSortDir,
            "sort_field": paramSortField
        };

        const headers = {
            // 'Authorization': 'Bearer my-token',
            'Content-type': 'application/json'
        };

        axios.post('http://localhost:8082/v1/api/product-group/category/list', paging, { headers })
            .then(response => {

                setCategories(response?.data?.data?.results);
                setTotalPage(response?.data?.data?.total_page);
            }).catch((error) => {
                console.log(error.response?.data?.error_code);
                if (error.response?.data?.error_code === '1004') {

                }
            });
    };


    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     */
    useEffect(() => {
        const timer = setTimeout(() => {
            findDataCategory(offset, limit, sortDir, sortField);
            setLoading(false);
        }, 1000);
        return () => clearTimeout(timer);

    }, []);

    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     * @param {*} e 
     * @returns 
     */
    const setSelectChangeLimit = (e) => {
        setLoading(true);
        setLimit(e.target.value);
        const timer = setTimeout(() => {
            findDataCategory(offset, e.target.value, sortDir, sortField);
            setLoading(false);
        }, 1000);
        return () => clearTimeout(timer);
    }

    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     * @param {*} e 
     * @returns 
     */
    const handleKeyDownPageInput = (e) => {
        if (e.key === 'Enter') {
            setLoading(true);
            if (e.target.value === 0) {
                console.log('vao day chua')
            } else {
                const timer = setTimeout(() => {
                    findDataCategory(e.target.value, limit, sortDir, sortField);
                    setLoading(false);
                }, 1000);
                return () => clearTimeout(timer);
            }
        }
    }

    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     * @param {*} e 
     * @param {*} value 
     * @returns 
     */
    const handleChangePage = (e, value) => {
        e.preventDefault();
        setOffset(value);
        setLoading(true);
        const timer = setTimeout(() => {
            findDataCategory(value, limit, sortDir, sortField);
            setLoading(false);
        }, 1000);
        return () => clearTimeout(timer);
    }

    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     */
    const clickToDeleteIds = () => {
        if (checked.length === 0) {
            toast.error('🦄 Vui long chon id truoc khi xoa!', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else {
            axios.delete('http://localhost:8082/v1/api/product-group/category/deletes', { data: checked })
                .then(async response => {
                    swal({
                        title: "Are you sure?",
                        text: "Are you sure that you want to leave this page?",
                        icon: "warning",
                        dangerMode: true,
                    })
                        .then(async willDelete => {
                            if (willDelete) {
                                setLoading(true);
                                toast.success('🦄 Xoa danh muc thanh cong!', {
                                    position: "top-right",
                                    autoClose: 2000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                                await setLoading(false);
                                await findDataCategory(offset, limit, sortDir, sortField);
                                await window.location.reload(false);
                            }
                        });

                })
                .catch((error) => {
                    console.log(error.response?.data);
                    if (error.response?.data.status === 400) {
                        window.location.reload(false);
                        findDataCategory(offset, limit, sortDir, sortField);
                        setChecked([]);
                    }
                });
        }
    }

    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     * @param {*} event 
     */
    const changeHandler = (event) => {
        setSelectedFile(event.target.files[0]);
        setImageUrl(event.target.files[0].name);
        setIsFilePicked(true);
    };

    const handleSubmitCreateData = (event) => {
        event.preventDefault();
        const formData = new FormData();

        formData.append('File', selectedFile);

        if (selectedFile?.name === undefined || isFilePicked === false) {
            toast.error('🦄 Vui long chon hinh anh!', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else if (categoryName === '') {
            toast.error('🦄 Vui long nhap ten danh muc!', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        } else {
            let objectCategory = {
                "category_name": categoryName,
                "seo_url": handleString(categoryName),
                "image_url": imageUrl,
                "record_created_by": "1" // User logged on system
            };
            // const uploadTask = storage.ref(`images/${imageUrl}`).put(imageUrl);
            // uploadTask.on('state_changed', 
            // (snapshot) => {
            // },
            // (error) => {
            // },
            // () => {
            //     storage.ref('images').child(imageUrl).getDownloadURL().then(url => {
            //         console.log(url)
            //     })
            // })
            axios.post('http://localhost:8082/v1/api/product-group/category/create', objectCategory)
                .then(response => {
                    console.log('asssas' + response?.data);
                    setCategoryName('');
                    setImageUrl('');
                    setSeoUrl('');
                    setSelectedFile('');
                    setIsFilePicked(false);
                    ref.current.value = "";
                    handleSubmitImageCategory();
                    toast.success('🦄 Them danh muc thanh cong!', {
                        position: "top-right",
                        autoClose: 2000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    findDataCategory(offset, limit, sortDir, sortField);
                })
                .catch((error) => {
                    if (error.response) {
                        toast.error('🦄 ' + error?.response?.data?.message, {
                            position: "top-right",
                            autoClose: 2000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                });
        }
    }

    /**
     * ANCHOR カテゴリー一覧
     * NOTE Archieve data list category with sort condition and paging number
     * TODO Archieve data list category with sort condition and paging number
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     */
    const handleSubmitImageCategory = () => {
        const formData = new FormData();

        formData.append('file', selectedFile);

        fetch(
            'http://localhost:8082/v1/api/files/upload',
            {
                method: 'POST',
                body: formData,
            }
        )
            .then((response) => response.json())
            .then((result) => {
                console.log('Success:', result);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    };

    /**
     * ANCHOR カテゴリー削除する。
     * NOTE Delete multi data category
     * TODO Delete multi data category
     * REVIEW 無し
     * FIXME 無し
     * STUB 無し
     * SECTION 無し
     * LINK 無し
     * 
     * @param {*} id 
     */
    const handleDeleteData = (id) => {
        axios.delete('http://localhost:8082/v1/api/product-group/category/delete/' + id)
            .then((response) => {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to leave this page?",
                    icon: "warning",
                    dangerMode: true,
                })
                    .then(willDelete => {
                        if (willDelete) {
                            if (response?.data?.status === 'OK' && response?.data?.data === 1) {
                                toast.success('🦄 Xoa danh muc thanh cong!', {
                                    position: "top-right",
                                    autoClose: 2000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: true,
                                    draggable: true,
                                    progress: undefined,
                                });
                                findDataCategory(offset, limit, sortDir, sortField);
                            }
                        }
                    });

            }).catch((error) => {
                console.error('Error:', error);
            });
    }


    /**
     * 
     * @param {*} event 
     */
    const handleCheckIdsToDelete = (event) => {
        var updatedList = [...checked];
        if (event.target.checked) {
            updatedList = [...checked, event.target.value];
        } else {
            updatedList.splice(checked.indexOf(event.target.value), 1);
        }
        setChecked(updatedList);
    }

    return (
        <PerfectScrollbar>
            <div className="container-scroller">
                <Navbar></Navbar>
                <div className="container-fluid page-body-wrapper">
                    <div className="row row-offcanvas row-offcanvas-right">
                        <Menu></Menu>
                        <div className="content-wrapper">
                            <div className='pb-4'>
                                <Button variant="contained" startIcon={<AddIcon />} data-toggle="modal" data-target="#exampleModal" color="secondary">
                                    新規登録
                                </Button>
                                &nbsp;&nbsp;&nbsp;
                                <Button variant="contained" startIcon={<DeleteIcon />} color="error" onClick={clickToDeleteIds}>
                                    削除
                                </Button>
                                &nbsp;&nbsp;&nbsp;
                                <Button variant="contained" startIcon={<DriveFileRenameOutlineIcon />} color="primary">
                                    編集
                                </Button>
                                &nbsp;&nbsp;&nbsp;
                                {/* <button type="button" className="btn btn-info">フィルター</button> */}
                                <div className="btn-group">
                                    <button type="button" className="btn btn-danger dropdown-toggle filter-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <FilterListIcon /> フィルター
                                    </button>
                                    <div className="dropdown-menu">
                                        <a className="dropdown-item" href="#">Action</a>
                                        <a className="dropdown-item" href="#">Another action</a>
                                        <a className="dropdown-item" href="#">Something else here</a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item" href="#">Separated link</a>
                                    </div>
                                </div>
                            </div>
                            <div className="card">
                                <h5 className="card-header header-panel">カテゴリー管理</h5>
                                <div className="card-body">
                                    {
                                        loading === true
                                            ?
                                            <p>loading...</p>
                                            :
                                            <table className="table table-bordered bg-white">
                                                <thead>
                                                    <tr>
                                                        <th style={{ "width": "3%" }} scope="col" className="custom-hd-tbl bg-info text-white"></th>
                                                        <th style={{ "width": "20%" }} scope="col" className="custom-hd-tbl bg-info text-white">カテゴリー名</th>
                                                        <th style={{ "width": "15%" }} scope="col" className="custom-hd-tbl bg-info text-white">記録作成時間</th>
                                                        <th style={{ "width": "8%" }} scope="col" className="custom-hd-tbl bg-info text-white">記録作成者</th>
                                                        <th style={{ "width": "15%" }} scope="col" className="custom-hd-tbl bg-info text-white">記録更新時間</th>
                                                        <th style={{ "width": "8%" }} scope="col" className="custom-hd-tbl bg-info text-white">記録更新者</th>
                                                        <th style={{ "width": "8%" }} scope="col" className="custom-hd-tbl bg-info text-white">アクション</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        categories?.map((item, key) => (
                                                            <tr key={key}>
                                                                <td>
                                                                    <Checkbox
                                                                        value={item?.category_code.toString()}
                                                                        id={item?.category_code.toString()}
                                                                        className='td-checkbox'
                                                                        onChange={handleCheckIdsToDelete}
                                                                        {...label} />
                                                                </td>
                                                                <td>
                                                                    {
                                                                        item?.is_display === 'no'
                                                                            ? <img style={{ width: "20px", height: "20px" }} src={require('../image/active/unactive-icon-removebg-preview.png')} alt={"active"}></img>
                                                                            : <img style={{ width: "20px", height: "20px" }} src='https://firebasestorage.googleapis.com/v0/b/ismjp-admin.appspot.com/o/active-icon-removebg-preview.png?alt=media&token=ee806d3c-affe-463f-8344-35c08e7baf56' alt={"unactive"}></img>
                                                                    }
                                                                    &nbsp;&nbsp;
                                                                    {
                                                                        item?.category_name
                                                                    }
                                                                </td>
                                                                <td> {moment(item?.record_created_at).format('YYYY年MM月DD日 HH時MM分SS秒')}</td>
                                                                <td>
                                                                    {item?.record_created_by === '1'
                                                                        ? 'ISMADMIN'
                                                                        : item?.record_created_by}
                                                                </td>
                                                                <td>{moment(item?.record_updated_at).format('YYYY年MM月DD日 HH時MM分SS秒')}</td>
                                                                <td>
                                                                    {item?.record_updated_by === '1'
                                                                        ? 'ISMADMIN'
                                                                        : item?.record_updated_by}
                                                                </td>
                                                                <td>
                                                                    <IconButton
                                                                        className="icon-active"
                                                                        size="small"
                                                                        aria-label="delete"
                                                                        color="primary">
                                                                        <DriveFileRenameOutlineIcon fontSize="small" />
                                                                    </IconButton>
                                                                    <IconButton
                                                                        className="icon-active"
                                                                        aria-label="delete"
                                                                        size="small"
                                                                        id="{item?.id}"
                                                                        onClick={() => handleDeleteData(item?.category_code)}
                                                                        color="error">
                                                                        <DeleteIcon fontSize="small" />
                                                                    </IconButton>
                                                                    <IconButton
                                                                        className="icon-active"
                                                                        aria-label="delete"
                                                                        size="small"
                                                                        color="success">
                                                                        <BrowseGalleryIcon fontSize="small" />
                                                                    </IconButton>
                                                                </td>
                                                            </tr>
                                                        ))
                                                    }
                                                </tbody>
                                            </table>
                                    }
                                    <br></br>
                                    <div className="d-flex pl-0 pr-0 justify-content-between">
                                        <div>
                                            <select className="form-control"
                                                style={{ border: "1px solid #E2E3E3" }}
                                                onChange={e => setSelectChangeLimit(e)}>
                                                <option value={5}>5</option>
                                                <option value={10}>10</option>
                                                <option value={15}>15</option>
                                                <option value={20}>20</option>
                                                <option value={50}>50</option>
                                                <option value={100}>100</option>
                                            </select>
                                        </div>
                                        <div>
                                            <div className="d-flex justify-content-end">
                                                <div className="col-sm-3">
                                                    <input
                                                        type="text"
                                                        style={{ border: "1px solid #E2E3E3" }}
                                                        className="form-control"
                                                        id="exampleInputEmail1"
                                                        aria-describedby="emailHelp"
                                                        placeholder="1"
                                                        onKeyDown={handleKeyDownPageInput}>
                                                    </input>
                                                </div>
                                                <div className="pt-1">
                                                    <Pagination
                                                        defaultPage={1}
                                                        count={Math.ceil(totalPage / limit)}
                                                        onChange={handleChangePage}
                                                        showFirstButton
                                                        showLastButton />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">追加カテゴリー</h5>
                            <button type="button" className="close" id="closeModalCreate" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        value={categoryName}
                                        className="form-control"
                                        id="inputAddress"
                                        autoFocus
                                        placeholder="Please input category name"
                                        onChange={((e) => setCategoryName(e.target.value))}></input>
                                </div>
                                <div className="form-group">
                                    <input type="file" name="file" onChange={changeHandler} ref={ref} />
                                </div>
                                <div className="form-group">
                                    <button type="button" className="btn btn-primary" onClick={handleSubmitCreateData}>追加</button>
                                    &nbsp;
                                    <button type="button" className="btn btn-warning" data-dismiss="modal">キャンセル</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal fade" id="errorDelete" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Alert Delete Category</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="form-group">
                                Please select id from checkbox
                            </div>
                            <div className="form-group">
                                <button type="button" className="btn btn-primary">追加</button>
                                &nbsp;
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer />
        </PerfectScrollbar>
    );
}

export default CategoryPage;