import './App.css';
import { BrowserRouter, Routes, Route, Link, Outlet } from "react-router-dom";
import { lazy, Suspense } from 'react';
import CategoryPage from './pages/CategoryPage';

const HomePage = lazy(() => import('./pages/HomePage'))
const ProductPage = lazy(() => import('./pages/ProductPage'))

function App() {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/product" element={<ProductPage />} />
          <Route path="/category" element={<CategoryPage />} />
        </Routes>
      </BrowserRouter>
    </Suspense>
  );
}

export default App;
