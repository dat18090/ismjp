import http from "../services/http-common";

const categoryURL = '/product-group/category'

const findDataCategoryActive = () => {
    return http.get(categoryURL + "/list");
};

const CategoryService = {
    findDataCategoryActive
};
export default CategoryService;