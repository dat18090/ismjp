function Menu() {
  return (
    <nav className="sidebar sidebar-offcanvas" id="sidebar">
      <ul className="nav">
        <li className="nav-item nav-profile">
          <div className="nav-link">
            <div className="profile-image">
              <img src='https://scontent.fsgn2-6.fna.fbcdn.net/v/t1.6435-9/81429489_475458310020589_4535775469357760512_n.jpg?_nc_cat=110&ccb=1-5&_nc_sid=174925&_nc_ohc=iE1gboBbV4oAX_oSM-p&_nc_ht=scontent.fsgn2-6.fna&oh=00_AT8SWFC649xGl-UmFQdFpfP5lWiQY7eK2taN1NDRKXFSsg&oe=6280BB42' alt="image"></img>
              <span className="online-status online"></span>
            </div>
            <div className="profile-name">
              <a href="https://www.facebook.com/profile.php?id=100026692726403">
                <p className="name">
                  ダットさん
                </p>
              </a>
              <p className="designation">
                Super Admin
              </p>
            </div>
          </div>
        </li>
        <li className="nav-item">
          <a className="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
            <i className="icon-target menu-icon"></i>
            <span className="menu-title">商品管理</span>
            <span className="badge badge-success">10</span>
          </a>
          <div className="collapse" id="ui-basic">
            <ul className="nav flex-column sub-menu">
              <li className="nav-item"> <a className="nav-link" href="pages\ui-features\accordions.html">サブカテゴリー</a></li>
              <li className="nav-item"> <a className="nav-link" href="/category">カテゴリー</a></li>
              <li className="nav-item"> <a className="nav-link" href="">銘柄</a></li>
              <li className="nav-item"> <a className="nav-link" href="pages\ui-features\breadcrumbs.html">銘柄のカテゴリー</a></li>
              <li className="nav-item"> <a className="nav-link" href="/product">商品</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </nav>
  );
}

export default Menu;